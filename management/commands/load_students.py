# -*- coding: utf-8 -*-
import os
import csv
from optparse import make_option
from django.core.management.base import BaseCommand
from django.core.management.base import CommandError
from django.utils import translation
from django.conf import settings
from apps.megacity.models import Student
from apps.merovingian.models import LEVEL_BA, LEVEL_MSC, LEVEL_PHD, LEVEL_U_MSC, Course, LEVEL_ENG


class Command(BaseCommand):
    args = '<filepath>'
    help = 'Load users from CVS file.'

    option_list = BaseCommand.option_list + (
        make_option('--insert',
            action='store_true',
            dest='insert',
            default=False,
            help='Insert students data'),
        make_option('--report',
            action='store_true',
            dest='report',
            default=False,
            help='Create loading report'),
        make_option('--manual-name-assignment',
            action='store',
            dest='manual-name-assignment-file',
            default=None,
            help='Path to file with manual assignment of courses\' names'),
        make_option('--save-not-found-courses',
            action='store',
            dest='not-found-courses-file',
            default=None,
            help='Path to output file with not found courses'),
        make_option('--save-multiple-courses',
            action='store',
            dest='multiple-courses-file',
            default=None,
            help='Path to output file with multiple courses'),
        make_option('--select-multiple-courses',
            action='store_true',
            dest='select-multiple-courses',
            default=False,
            help=''),
        )

    def select_one_course(self, student_data, courses):
        print u"Select course for '{0}'".format(u', '.join([unicode(x, encoding='utf-8') for x in student_data[3:7]]))
        print u"  0. None"
        i = 1
        for c in courses:
            print u"  {0}. {1}".format(unicode(i), unicode(c))
            i += 1
        index = -1
        while index < 0:
            inp = raw_input('')
            try:
                inp = int(inp)
                if inp > len(courses):
                    continue
            except Exception, e:
                continue
            index = inp
        save_ans = raw_input(u'Choose it for the same course in the future? [Y/n]').strip().lower()
        save = save_ans in ['', 'y']
        if index == 0:
            return None, save
        else:
            return courses[index-1], save

    def find_course(self, student_data, manual_name_assignments):

        levels_map = {
            'pierwszy': [LEVEL_BA, LEVEL_ENG],
            'drugi': [LEVEL_MSC],
            'doktoranckie': [LEVEL_PHD],
            'jednolite magisterskie': [LEVEL_U_MSC]
        }
        level_name = student_data[4].strip().lower()
        if level_name in levels_map:
            level_ids = levels_map[level_name]
        else:
            return []

        types_map = {
            'zaoczne': 'niestacjonarny',
            'wieczorowe': 'niestacjonarny',
            'stacjonarne': 'stacjonarny',
            'niestacjonarne': 'niestacjonarny'
        }
        type_name = student_data[5].strip().lower()
        if type_name in types_map:
            type_name = types_map[type_name]
        else:
            return []

        course_year = int(student_data[6])

        course_name = student_data[3]
        if course_name in manual_name_assignments:
            course_name = manual_name_assignments[course_name]
            course_name = course_name.lower()
        else:
            course_name = course_name.lower().split(',')[0].strip()

        courses = Course.objects.filter(name__iexact=course_name)\
            .filter(start_date__year=course_year)\
            .filter(type__name__iexact=type_name)\
            .filter(level__id__in=level_ids)

        if len(student_data) >= 8:
            course_profile = student_data[7].strip().lower()
            if len(course_profile) > 0:
                courses = courses.filter(profile__name__iexact=course_profile)
        return courses


    def handle(self, *args, **options):

        translation.activate(getattr(settings, 'LANGUAGE_CODE', settings.LANGUAGE_CODE))

        if len(args) != 1:
            raise CommandError('Wrong number of parameters. Expected 1: ' + self.args)

        if (options['report'] and options['insert']) or not (options['report'] or options['insert']):
            raise CommandError('Wrong options. You need to select either --report or --insert.')

        filepath = args[0]
        if not os.path.isfile(filepath):
            raise CommandError('File "{0}" does not exist.'.format(filepath))

        all_rows = 0
        wrong_columns_number = 0
        course_not_found = 0
        multiple_courses = 0
        relation_added = 0
        new_students = 0
        existing_students = 0

        select_multiple_map = {}

        manual_name_assignments = {}
        if options['manual-name-assignment-file'] is not None and os.path.isfile(options['manual-name-assignment-file']):
            reader = csv.reader(open(options['manual-name-assignment-file']), delimiter=',', quotechar='"')
            for data in reader:
                if len(data) == 2:
                    manual_name_assignments[data[0]] = data[1]

        course_not_found_writer = None
        if options['not-found-courses-file'] is not None:
            course_not_found_writer = csv.writer(open(options['not-found-courses-file'], 'w'), delimiter=',', quotechar='"')

        multiple_courses_writer = None
        if options['multiple-courses-file'] is not None:
            multiple_courses_writer = csv.writer(open(options['multiple-courses-file'], 'w'), delimiter=',', quotechar='"')

        studens_reader = csv.reader(open(filepath), delimiter=',', quotechar='"')
        for student_data in studens_reader:
            all_rows += 1
            if len(student_data) < 7:
                wrong_columns_number += 1
                continue

            courses = self.find_course(student_data, manual_name_assignments)
            if len(courses) == 0:
                course_not_found += 1
                if course_not_found_writer is not None:
                    course_not_found_writer.writerow(student_data)
                continue
            elif len(courses) > 1:
                course = None
                if options['select-multiple-courses']:
                    hash_key = '_'.join([unicode(x, encoding='utf-8').strip() for x in student_data[3:7]])
                    if hash_key in select_multiple_map:
                        if select_multiple_map[hash_key] is not None:
                            course = select_multiple_map[hash_key]
                    else:
                        course, save = self.select_one_course(student_data, courses)
                        if save:
                            select_multiple_map[hash_key] = course

                if course is not None:
                    relation_added += 1
                else:
                    multiple_courses += 1
                    if multiple_courses_writer is not None:
                        multiple_courses_writer.writerow(student_data)
                    continue
            else:
                relation_added += 1
                course = courses[0]

            last_name = student_data[0].strip()
            first_name = student_data[1].strip()
            email = student_data[2].strip()

            students = Student.objects.filter(email=email)
            if len(students) == 0:
                new_students += 1

                if options['insert']:
                    student = Student()
                    student.first_name = first_name
                    student.last_name = last_name
                    student.email = email
                    student.save()
            else:
                existing_students += 1

                student = students[0]

            if options['insert']:
                student.courses.add(course)

        if options['report']:
            print "##################################"
            print "#        REPORT                   "
            print "##################################"
            print ""
            print "All rows:               {0}".format(all_rows)
            print "Wrong format:           {0}".format(wrong_columns_number)
            print "Course not found:       {0}".format(course_not_found)
            print "Multiple courses found: {0}".format(multiple_courses)
            print "Relations added:        {0}".format(relation_added)
            print ""
            print "New students:       {0}".format(new_students)
            print "Existing students:  {0}".format(existing_students)


