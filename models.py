# -*- coding: utf-8 -*-

from django.db import models
from django.utils.translation import ugettext_lazy as _
from apps.merovingian.models import Course


class Student(models.Model):

    first_name = models.CharField(max_length=128, verbose_name=_(u"First name"))
    last_name = models.CharField(max_length=128, verbose_name=_(u"Last name"))

    email = models.CharField(max_length=128, verbose_name=_(u"E-mail"))

    courses = models.ManyToManyField(Course, related_name='students')